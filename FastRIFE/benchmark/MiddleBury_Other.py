import sys
sys.path.append('.')
import cv2
import math
import torch
import numpy as np
import time

# from model.FastRIFE_GF import Model
# model_path = r'model_GF'

from model.FastRIFE_LK import Model
model_path = r'model_LK'

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

model = Model()
model.load_model(model_path)
model.eval()
model.device()
path = r'MiddleBury/'

name = ['Beanbags', 'Dimetrodon', 'DogDance', 'Grove2', 'Hydrangea', 'MiniCooper', 'RubberWhale', 'Urban2', 'Urban3', 'Venus', 'Walking']
IE_list = []
times = []
psnr_all = []
for i in name:
    i0 = cv2.imread(path + 'other-data/{}/frame10.png'.format(i)).transpose(2, 0, 1) / 255.
    i1 = cv2.imread(path + 'other-data/{}/frame11.png'.format(i)).transpose(2, 0, 1) / 255.
    gt = cv2.imread(path + 'other-gt-interp/{}/frame10i11.png'.format(i))
    h, w = i0.shape[1], i0.shape[2]
    imgs = torch.zeros([1, 6, 480, 640]).to(device)
    ph = (480 - h) // 2
    pw = (640 - w) // 2
    imgs[:, :3, :h, :w] = torch.from_numpy(i0).unsqueeze(0).float().to(device)
    imgs[:, 3:, :h, :w] = torch.from_numpy(i1).unsqueeze(0).float().to(device)
    I0 = imgs[:, :3]
    I2 = imgs[:, 3:]
    start = time.time()
    pred = model.inference(I0, I2)
    end = time.time()
    out = pred[0].detach().cpu().numpy().transpose(1, 2, 0)
    out = np.round(out[:h, :w] * 255)
    IE = np.mean(np.abs(out - gt * 1.0))
    IE_list.append(IE)
    cur_time = (end - start) * 1000
    times.append(cur_time)
    mse = np.mean((out - gt * 1.0) ** 2)
    PIXEL_MAX = 255.0
    psnr = 20 * math.log10(PIXEL_MAX / math.sqrt(mse))
    psnr_all.append(psnr)
    print('======', i, '=======')
    print('IE:', IE, 'PSNR:', psnr, 'time:', cur_time)


print('===========AVERAGE===========')
print('IE:', np.mean(IE_list))
print('PSNR', np.mean(psnr_all))
print('time:', np.mean(times))
