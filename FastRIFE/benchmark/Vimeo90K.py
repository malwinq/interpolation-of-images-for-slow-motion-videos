import sys
sys.path.append('.')
import cv2
import math
import torch
import numpy as np
import time
from pytorch_msssim import ssim_matlab
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def count_parameters(model):
    res = 0
    for net in [model.contextnet, model.fusionnet]:
        res += sum(p.numel() for p in net.parameters() if p.requires_grad)
    return res

from model.FastRIFE_LK import Model
model = Model()
model.load_model(r'model_LK')
model.eval()
model.device()
print('Number of params: ', count_parameters(model))


path = r'dataset/vimeo_triplet/'
f = open(path + 'tri_testlist.txt', 'r')
psnr_list = []
ssim_list = []
times = []
for i in f:
    name = str(i).strip()
    if(len(name) <= 1):
        continue
    print(path + 'sequences/' + name + '/im1.png')
    I0 = cv2.imread(path + 'sequences/' + name + '/im1.png')
    I1 = cv2.imread(path + 'sequences/' + name + '/im2.png')
    I2 = cv2.imread(path + 'sequences/' + name + '/im3.png')
    I0 = (torch.tensor(I0.transpose(2, 0, 1)).to(device) / 255.).unsqueeze(0)
    I2 = (torch.tensor(I2.transpose(2, 0, 1)).to(device) / 255.).unsqueeze(0)
    start = time.time()
    mid = model.inference(I0, I2)[0]
    end = time.time()
    mid = mid.type(torch.FloatTensor).to(device)
    ssim = ssim_matlab(torch.tensor(I1.transpose(2, 0, 1)).to(device).unsqueeze(0) / 255., torch.round(mid * 255).unsqueeze(0) / 255.).detach().cpu().numpy()
    mid = np.round((mid * 255).detach().cpu().numpy()).astype('uint8').transpose(1, 2, 0) / 255.    
    I1 = I1 / 255.
    psnr = -10 * math.log10(((I1 - mid) * (I1 - mid)).mean())
    times.append((end-start)*1000)
    psnr_list.append(psnr)
    ssim_list.append(ssim)
    print("Avg PSNR: {} SSIM: {} time: {}".format(np.mean(psnr_list), np.mean(ssim_list), np.mean(times)))
