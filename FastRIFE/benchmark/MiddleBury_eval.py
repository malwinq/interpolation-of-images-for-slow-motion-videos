import os
import sys
sys.path.append('.')
import cv2
import torch
from model.FastRIFE_GF import Model
import time

path = r'model_GF'

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

model = Model()
model.load_model(path)
model.eval()
model.device()

data_path = r'MiddleBury/eval-data/'
output_path = r'MiddleBury/eval-result/'
for subdir in os.listdir(data_path):
    i0 = cv2.imread(data_path + subdir + r'/frame10.png').transpose(2, 0, 1) / 255.
    i1 = cv2.imread(data_path + subdir + r'/frame11.png').transpose(2, 0, 1) / 255.
    h, w = i0.shape[1], i0.shape[2]
    imgs = torch.zeros([1, 6, 480, 640]).to(device)
    ph = (480 - h) // 2
    pw = (640 - w) // 2
    imgs[:, :3, :h, :w] = torch.from_numpy(i0).unsqueeze(0).float().to(device)
    imgs[:, 3:, :h, :w] = torch.from_numpy(i1).unsqueeze(0).float().to(device)
    I0 = imgs[:, :3]
    I2 = imgs[:, 3:]

    start = time.time()
    mid = model.inference(I0, I2)
    end = time.time()

    print(subdir, (end-start) * 1000)

