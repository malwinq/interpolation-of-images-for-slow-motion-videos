# FastRIFE

![Image](images/title_photo.png)

## Video Frame Interpolation

Master thesis: The analysis of algorithms for interpolation of images for creating slow motion videos

The problem of video inter-frame interpolation is an essential task in the field of image processing. Correctly increasing the number of frames in the recording while maintaining smooth movement allows to improve the quality of played video sequence, enables more effective compression and creating a slow-motion recording. This paper proposes the FastRIFE algorithm, which is some speed improvement of the RIFE (Real-Time Intermediate Flow Estimation) model. The novel method was examined and compared with other recently published algorithms.

## Proposed solution

Most state-of-the-art algorithms generate intermediate frames by combining bi-directional optical flows and additional networks, e.g. estimating depth, which makes these methods unable to real-time work. Huang et al. proposed in RIFE a solution of simple neural network for estimating optical flows called IFNet and a network for generating interpolated frames called FusionNet.

In this paper authors will analyze the RIFE model with different module for estimating optical flow. Although the IFNet gives excellent results and runs very fast, authors wanted to test analytical methods and compare the results of such a simplified model in terms of runtime and quality. This change can speed up the algorithm even more, making the whole process capable of running real-time on many more devices. 

Authors replaced the IFNet part with analytical methods: Gunner-Farneback and Lucas-Kanade. Then, new FusionNet and ContextNet models were fine-tuned on the proposed solution. 

![Image](images/structure.png)

## Compared algorithms

![Image](images/visual_compare.png)

[1] RIFE - 2021

https://arxiv.org/abs/2011.06294

[2] Softmax splatiing - 2020

https://arxiv.org/pdf/2003.05534v1.pdf

[3] DAIN (Depth-Aware Video Frame Interpolation) - 2019

https://arxiv.org/pdf/1904.00830.pdf

[4] MEMC-Net (Motion Estimation and Motion Compensation Driven Neural Network for Video Interpolation and Enhancement) - 2019

https://arxiv.org/pdf/1810.08768.pdf

[5] Super SloMo (Deep Slow Motion Video Reconstruction with Hybrid Imaging System) - 2018

https://arxiv.org/pdf/1712.00080.pdf

[6] SepConv - 2017

https://arxiv.org/pdf/1708.01692.pdf


## Datasets

- Middlebury: https://vision.middlebury.edu/flow/floweval-ijcv2011.pdf - test, 'Evaluation' and 'Other' subsets

- UCF101: https://www.crcv.ucf.edu/data/UCF101.php - test

- Vimeo90K: http://data.csail.mit.edu/tofu/dataset/vimeo_triplet.zip - used for training AdaCoF and DAIN


## Measures

- IE (Interpolation Error), NIE (Normalized Interpolation Error) - Middlebury Evaluation

- PSNR (Peak Signal to-Noise Ratio), SSIM (Structural Similarity) - Middlebury Other, UCF101, Vimeo90K


## Hardware

GPU Nvidia GeForce RTX 2080 Ti
